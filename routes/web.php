<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return redirect()->route('clients.index');
});

Route::namespace('Clients')->prefix('cliente')->group(function () {
	Route::get('/listar/{active_slug?}', 'ClientsController@index')->name('clients.index');
	Route::get('/cadastrar', 'ClientsController@create')->name('clients.create');
	Route::post('/cadastrar', 'ClientsController@store')->name('clients.store');
	Route::get('/{id}/editar', 'ClientsController@edit')->name('clients.edit');
	Route::put('/{id}/editar', 'ClientsController@update')->name('clients.update');
	Route::get('/{id}/visualizar', 'ClientsController@show')->name('clients.show');
	Route::delete('/{id}', 'ClientsController@destroy')->name('clients.destroy');
});

Route::namespace('Logs')->prefix('log')->group(function () {
	Route::get('/listar/{event_slug?}', 'LogsController@index')->name('logs.index');
});
