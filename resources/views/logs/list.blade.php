@extends('adminlte::page')

@section('title', 'Listagem de logs')

@section('content_header')
	<h1>Logs -
		<small>listagem</small>
	</h1>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-1">
							<h3 class="box-title">Logs</h3>
						</div>
						<div class="col-md-10">
							<a class="btn btn-xs btn-default" href="{{ route('logs.index') }}">
								Todos
							</a>
							<a class="btn btn-xs btn-default" href="{{ route('logs.index', ['event_slug' => 'created']) }}">
								Criados
							</a>
							<a class="btn btn-xs btn-default" href="{{ route('logs.index', ['event_slug' => 'updated']) }}">
								Alterados
							</a>
							<a class="btn btn-xs btn-default" href="{{ route('logs.index', ['event_slug' => 'deleted']) }}">
								Deletados
							</a>
						</div>
					</div>
				</div>
				<div class="box-body">
					<table class="table table-striped table-hover dataTable" id="table-logs" role="grid">
						<thead>
						<tr>
							<th>#</th>
							<th>Classe</th>
							<th>Evento</th>
							<th>Dados</th>
						</tr>
						</thead>
						<tfoot>
						<tr>
							<th>#</th>
							<th>Classe</th>
							<th>Evento</th>
							<th>Dados</th>
						</tr>
						</tfoot>
						<tbody>
							@foreach($logs as $log)
								<tr>
									<td>{{ $log->id }}</td>
									<td>{{ $log->class }} - #{{ $log->class_id }}</td>
									<td>{{ $log->event }}</td>
									<td>{{ $log->data }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop

@section('js')
	<script>
		$(document).ready(function () {
			$('#table-logs').DataTable({
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
				}
			});
		});
	</script>
@stop