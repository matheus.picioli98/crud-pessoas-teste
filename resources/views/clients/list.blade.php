@extends('adminlte::page')

@section('title', 'Listagem de usuários')

@section('content_header')
	<h1>Usuários -
		<small>listagem</small>
	</h1>
@stop

@section('content')
	<div class="row">
		<div class="col-md-1">
			<a href="{{ route('clients.create') }}" class="btn btn-success">
				<i class="fa fa-plus"></i> Cadastrar
			</a>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-1">
							<h3 class="box-title">Usuários</h3>
						</div>
						<div class="col-md-10">
							<a class="btn btn-xs btn-default" href="{{ route('clients.index', ['active_slug' => 'ativos']) }}">
								Ativos
							</a>
							<a class="btn btn-xs btn-default" href="{{ route('clients.index', ['active_slug' => 'inativos']) }}">
								Inativos
							</a>
						</div>
					</div>
				</div>
				<div class="box-body">
					<table class="table table-striped table-hover dataTable" id="table-clients" role="grid">
						<thead>
						<tr>
							<th>#</th>
							<th>Nome</th>
							<th>Sobrenome</th>
							<th>Apelido</th>
							<th>Ativo</th>
							<th>Ações</th>
						</tr>
						</thead>
						<tfoot>
						<tr>
							<th>#</th>
							<th>Nome</th>
							<th>Sobrenome</th>
							<th>Apelido</th>
							<th>Ativo</th>
							<th>Ações</th>
						</tr>
						</tfoot>
						<tbody>
						@foreach($clients as $client)
						<tr>
							<td>{{ $client->id }}</td>
							<td>{{ $client->name }}</td>
							<td>{{ $client->last_name }}</td>
							<td>{{ $client->nickname ?? '-' }}</td>
							<td>{{ $client->active_text }}</td>
							<td>
								<a class="btn btn-xs btn-primary"
								   href="{{ route('clients.show', ['id' => $client->id]) }}">
									<!-- @todo implements link for edit -->
									<i class="fa fa-eye"></i>
								</a>
								<a class="btn btn-xs btn-warning"
								   href="{{ route('clients.edit', ['id' => $client->id]) }}">
									<!-- @todo implements link for edit -->
									<i class="fa fa-pencil"></i>
								</a>
								<button type="button" data-toggle="modal"
										data-target="#modal-danger-{{ $client->id }}"
										href="#" class="btn btn-xs btn-danger">
									<i class="fa fa-trash"></i>
								</button>
								<!-- FORM ACTIVE/DISABLE -->
								<form action="{{ route('clients.update', ['id' => $client->id]) }}" method="POST">
									{{ csrf_field() }}
									{{ method_field('put') }}
									@if ($client->active)
										<input type="hidden" name="active" value="0">
										<br>
										<button class="btn btn-xs btn-danger" type="submit">
											<!-- @todo if to check if active client -->
											Desativar <i class="fa fa-lock"></i>
										</button>
									@else
										<input type="hidden" name="active" value="1">
										<br>
										<button class="btn btn-xs btn-success" type="submit">
											<!-- @todo if to check if active client -->
											Ativar <i class="fa fa-lock"></i>
										</button>
									@endif
								</form>
								<!-- FORM ACTIVE/DISABLE -->
								<!-- MODAL EXCLUSÃO -->
								<div id="modal-danger-{{ $client->id }}" class="modal modal-danger fade">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button class="close" type="button" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">×</span>
												</button>
												<h3 class="modal-title">Confirmar exclusão</h3>
											</div>
											<div class="modal-body">
												<h4>Deseja realmente excluir o cliente {{ $client->name }}?</h4>
											</div>
											<div class="modal-footer">
												<button class="btn btn-outline pull-left" type="button" data-dismiss="modal">
													Fechar
												</button>
												<form method="POST"
													  action="{{ route('clients.destroy', ['id' => $client->id]) }}">
													{{ csrf_field() }}
													{{ method_field('DELETE') }}
													<button class="btn btn-outline" type="submit">Confirmar exclusão</button>
												</form>
											</div>
										</div>
									</div>
								</div>
								<!-- MODAL EXCLUSÃO -->
							</td>
						</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop

@section('js')
	<script>
		$(document).ready(function () {
			$('#table-clients').DataTable({
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
				}
			});
		});
	</script>
@stop