@extends('adminlte::page')

@section('title', 'Visualização de usuário')

@section('content_header')
	<h1>Usuário -
		<small>visualizar</small>
	</h1>
@stop

@section('content')
	<div class="row">
		<div class="col-md-1">
			<a href="{{ route('clients.index') }}" class="btn btn-default">
				<i class="fa fa-rotate-left"></i> Voltar</a>
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Visualização de usuário</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<table class="table table-striped table-hover">
								<tr>
									<th><p class="text-bold">ID: </p></th>
									<td>#{{ $client->id }}</td>
								</tr>
								<tr>
									<th><p class="text-bold">Nome: </p></th>
									<td>{{ $client->name }}</td>
								</tr>
								<tr>
									<th><p class="text-bold">Sobrenome: </p></th>
									<td>{{ $client->last_name }}</td>
								</tr>
								<tr>
									<th><p class="text-bold">Apelido: </p></th>
									<td>{{ $client->nickname ?? '-' }}</td>
								</tr>
								<tr>
									<th><p class="text-bold">Data nascimento: </p></th>
									<td>{{ $client->birthday->format('d-m-Y') }}</td>
								</tr>
								<tr>
									<th><p class="text-bold">Ativo: </p></th>
									<td>{{ $client->active_text }}</td>
								</tr>
								<tr>
									<th><p class="text-bold">Telefones: </p></th>
									<td>
										@forelse($client->celphones as $celphone)
											<p>{{ $celphone->number }}</p>
										@empty
											Não há telefones cadastrados
										@endforelse
									</td>
								</tr>
								<tr>
									<th><p class="text-bold">Endereços: </p></th>
									<td>
										@forelse($client->address as $address)
											<p>{{ $address->street }}, {{ $address->number }} - {{ $address->neighborhood }}</p>
										@empty
											Não há endereços cadastrados
										@endforelse
									</td>
								</tr>
							</table>
							<a href="{{ route('clients.edit', ['id' => $client->id]) }}" class="btn btn-warning">
								<i class="fa fa-pencil"></i> Editar
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop