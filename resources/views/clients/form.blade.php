@extends('adminlte::page')

@section('title', 'Cadastro de usuários')

@section('content_header')
	<h1>Usuários -
		<small>Cadastrar</small>
	</h1>
@stop

@section('content')
	<div class="row">
		<div class="col-md-1">
			<a href="{{ route('clients.index') }}" class="btn btn-default">
				<i class="fa fa-rotate-left"></i> Voltar</a>
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Cadastro de usuário</h3>
				</div>
				<div class="box-body">
					@if (isset($client))
					<form action="{{ route('clients.update', ['id' => $client->id]) }}" method="POST">
						{{ csrf_field() }}
						{{ method_field('put') }}
					@else
					<form action="{{ route('clients.store') }}" method="POST">
						{{ csrf_field() }}
					@endif
						<div class="row">
							<div class="col-md-3">
								<div class="form-group @if($errors->has('name')) has-error @endif">
									@component('inputs.String', [
										'nome' 			=> 'name',
										'id'			=> 'name',
										'valor'			=> old('name') ?? $client->name ?? null,
										'tabindex'		=> 1,
										'texto'			=> 'Nome*',
										'titulo'        => 'Nome'
									])@endcomponent
									@if( $errors->has('name') )
										<span style="color: #f56954">{{ $errors->get('name')[0] }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group @if($errors->has('last_name')) has-error @endif">
									@component('inputs.String', [
										'nome' 			=> 'last_name',
										'id'			=> 'last_name',
										'valor'			=> old('last_name') ?? $client->last_name ?? null,
										'tabindex'		=> 2,
										'texto'			=> 'Sobrenome*',
										'titulo'        => 'Sobrenome'
									])@endcomponent
									@if( $errors->has('last_name') )
										<span style="color: #f56954">{{ $errors->get('last_name')[0] }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group @if($errors->has('nickname')) has-error @endif">
									@component('inputs.String', [
										'nome' 			=> 'nickname',
										'id'			=> 'nickname',
										'valor'			=> old('nickname') ?? $client->nickname ?? null,
										'tabindex'		=> 3,
										'texto'			=> 'Apelido',
										'titulo'        => 'Apelido'
									])@endcomponent
									@if( $errors->has('nickname') )
										<span style="color: #f56954">{{ $errors->get('nickname')[0] }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group @if($errors->has('birthday')) has-error @endif">
									@component('inputs.Data', [
										'nome' 			=> 'birthday',
										'id'			=> 'birthday',
										'valor'			=> old('birthday') ?? (isset($client) ? $client->birthday->format('Y-m-d') : null) ?? null,
										'tabindex'		=> 4,
										'texto'			=> 'Data nascimento*',
										'titulo'        => 'Data nascimento'
									])@endcomponent
									@if( $errors->has('birthday') )
										<span style="color: #f56954">{{ $errors->get('birthday')[0] }}</span>
									@endif
								</div>
							</div>
						</div>

						<!-- CADASTRO TELEFONES -->
						<div class="row">
							<div class="col-md-12">
								<div class="box box-info box-solid">
									<div class="box-header">
										<h3 class="box-title">Telefones</h3>
									</div>
									<div class="box-body">
										<div class="row">
											<div id="telephones">
											@if(isset($client))
												@forelse($client->celphones as $celphone)
													<div class="col-md-3">
														<div class="form-group">
															<input type="text" id="telephones[{{ ($loop->iteration - 1)}}][number]"
															   name="telephones[{{ ($loop->iteration - 1) }}][number]"
															   value="{{ $celphone->number }}"
															   class="form-control"
															   placeholder="Apenas números">
															<input
																type="hidden"
																name='telephones[{{ ($loop->iteration - 1) }}][id]'
																id='telephones[{{ ($loop->iteration - 1) }}][id]'
																value='{{ $celphone->id }}'>
														</div>
													</div>
												@empty
													<div class="col-md-3">
														<div class="form-group">
															<input type="text" id="telephones[0]"
																   name="telephones[0]" class="form-control"
																   placeholder="Apenas números">
														</div>
													</div>
												@endforelse
											@else
												<div class="col-md-3">
													<div class="form-group">
														<input type="text" id="telephones[0]"
														   name="telephones[0]" class="form-control"
														   placeholder="Apenas números">
													</div>
												</div>
											@endif
											</div>
										</div>
										@if(!isset($client))
											<button type="button" class="btn btn-info add_telephone_button">
												<i class="fa fa-plus"></i> Adicionar telefone
											</button>
										@endif
									</div>
								</div>
							</div>
						</div>
						<!-- CADASTRO DE TELEFONES -->

						<!-- CADASTRO DE ENDEREÇOS -->
						<div class="row">
							<div class="col-md-12">
								<div class="box box-warning box-solid">
									<div class="box-header">
										<h3 class="box-title">Endereços</h3>
									</div>
									<div class="box-body">
										<div id="address">
											@if(isset($client))
												@forelse($client->address as $address)
													<div class="row">
														<div class="col-md-4">
															<div class="form-group">
																<input type="text"
																	   class="form-control"
																	   id="address[{{ ($loop->iteration - 1) }}][street]"
																	   name="address[{{ ($loop->iteration - 1) }}][street]"
																	   placeholder="Digite o nome da rua"
																	   value="{{ $address->street }}" />
															</div>
														</div>
														<div class='col-md-4'>
															<div class='form-group'>
																<input type='number'
																	   class='form-control'
																	   id='address[{{ ($loop->iteration - 1) }}][number]'
																	   name='address[{{ ($loop->iteration - 1) }}][number]'
																	   placeholder='Digite o nº da casa'
																	   value="{{ $address->number }}" />
															</div>
														</div>
														<div class='col-md-4'>
															<div class='form-group'>
																<input type='text'
																	   class='form-control'
																	   id='address[{{ ($loop->iteration - 1) }}][neighborhood]'
																	   name='address[{{ ($loop->iteration - 1) }}][neighborhood]'
																	   placeholder='Digite o bairro'
																	   value="{{ $address->neighborhood }}" />
																<input
																	type="hidden"
																	name='address[{{ ($loop->iteration - 1) }}][id]'
																	id='address[{{ ($loop->iteration - 1) }}][id]'
																	value='{{ $address->id }}'>
															</div>
														</div>
													</div>
												@empty
													<div class="row">
														<div class="col-md-4">
															<div class="form-group">
																<input type="text"
																	   class="form-control"
																	   id="address[0][street]"
																	   name="address[0][street]" placeholder="Digite o nome da rua" />
															</div>
														</div>
														<div class='col-md-4'>
															<div class='form-group'>
																<input type='number'
																	   class='form-control'
																	   id='address[0][number]'
																	   name='address[0][number]' placeholder='Digite o nº da casa' />
															</div>
														</div>
														<div class='col-md-4'>
															<div class='form-group'>
																<input type='text'
																	   class='form-control'
																	   id='address[0][neighborhood]'
																	   name='address[0][neighborhood]' placeholder='Digite o bairro' />
															</div>
														</div>
													</div>
												@endforelse
											@else
												<div class="row">
													<div class="col-md-4">
														<div class="form-group">
															<input type="text"
																   class="form-control"
																   id="address[0][street]"
																   name="address[0][street]" placeholder="Digite o nome da rua" />
														</div>
													</div>
													<div class='col-md-4'>
														<div class='form-group'>
															<input type='number'
																   class='form-control'
																   id='address[0][number]'
																   name='address[0][number]' placeholder='Digite o nº da casa' />
														</div>
													</div>
													<div class='col-md-4'>
														<div class='form-group'>
															<input type='text'
																   class='form-control'
																   id='address[0][neighborhood]'
																   name='address[0][neighborhood]' placeholder='Digite o bairro' />
														</div>
													</div>
												</div>
											@endif
										</div>
										@if(!isset($client))
											<button type="button" class="btn btn-info add_address_button">
												<i class="fa fa-plus"></i> Adicionar endereço
											</button>
										@endif
									</div>
								</div>
							</div>
						</div>
						<!-- CADASTRO DE ENDEREÇOS -->

						<div class="row">
							<div class="col-md-offset-11 col-md-1">
								<div class="form-group">
									@component('inputs.Botao', [
										'id'		=> 'save',
										'classes'	=> 'btn-info',
										'texto'		=> ' Salvar',
										'icone'     => 'fa fa-save',
										'titulo'    => 'Clique aqui para salvar'
									])@endcomponent
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('js')
	<script>
		$(document).ready(function () {
			var telephones = $('#telephones');
			var address = $('#address');
			var button_telephone = $('.add_telephone_button');
			var button_address = $('.add_address_button');
			var count = 0;
			var count_address = 0;

			$(button_telephone).click(function (event) {
				event.preventDefault();
				if (count < 1) {
					// using template string
					var button_add_input = '';
					if (count === 0) {
						button_add_input = '<a href="#" class="btn btn-danger remove_field"><i class="fa fa-times"></a></div>';
					}
					count++;
					var telephoneNumber = `telephones[${count}]`;
					var input_celphone = "<div class='col-md-3'><div class='form-group'><input type='text' id="+telephoneNumber+" name="+telephoneNumber+" class='form-control'></div></div>";
					$(telephones).append(input_celphone+button_add_input);
				} else {
					window.alert('Você só pode adicionar 2 telefones');
				}
			});

			$(telephones).on('click', '.remove_field', function (event) {
				/**
				 * @todo parent().remove() removing all the telephones
				 */
				event.preventDefault();
				console.log(count);
				console.log($(this).parent().children());
				var element = $(this).parent().children()[count];
				element.remove();
				$(this).remove();
				count--;
			});

			$(button_address).click(function (event) {
				event.preventDefault();
				if (count_address < 1) {
					var button_remove = '';
					if (count_address === 0) {
						button_remove= '<a href="#" class="btn btn-danger remove_field"><i class="fa fa-times"></a></div>';
					}
					count_address++;
					var inputs = "<div class='row'>";
					inputs += `<div class='col-md-4'><div class='form-group'><input type='text' class='form-control' id='address[${count_address}][street]' name='address[${count_address}][street]' placeholder='Digite o nome da rua' /></div></div>`;
					inputs += `<div class='col-md-4'><div class='form-group'><input type='number' class='form-control' id='address[${count_address}][number]' name='address[${count_address}][number]' placeholder='Digite o nº da casa' /></div></div>`;
					inputs += `<div class='col-md-4'><div class='form-group'><input type='text' class='form-control' id='address[${count_address}][neighborhood]' name='address[${count_address}][neighborhood]' placeholder='Digite o bairro' /></div></div>`;
					inputs += "</div>";
					$(address).append(inputs+button_remove);
				} else {
					window.alert('Você só pode adicionar 2 endereços');
				}
			});

			$(address).on('click', '.remove_field', function (event) {
				/**
				 * @todo parent().remove() removing all the telephones
				 */
				event.preventDefault();
				var element = $(this).parent().children()[count_address];
				element.remove();
				$(this).remove();
				count_address--;
			});
		});
	</script>
@stop