<?php

namespace OGestor\Models\Clients;

use OGestor\Models\Address\AddressModel;
use OGestor\Models\Contacts\CelphoneModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientModel extends Model
{
	use SoftDeletes;
	
	protected $table = 'clients';
	
	protected $fillable = [
		'name',
		'last_name',
		'nickname',
		'birthday',
		'active',
	];
	
	protected $casts = [
		'active' => 'boolean',
	];
	
	protected $dates = [
		'birthday',
	];
	
	public function getActiveTextAttribute ()
	{
		return $this->active ? 'Sim' : 'Não';
	}
	
	public function scopeActives ($query, bool $value)
	{
		return $query->where('active', $value);
	}
	
	public function address ()
	{
		return $this->hasMany(AddressModel::class, 'client_id', 'id');
	}
	
	public function celphones ()
	{
		return $this->hasMany(CelphoneModel::class, 'client_id', 'id');
	}
}
