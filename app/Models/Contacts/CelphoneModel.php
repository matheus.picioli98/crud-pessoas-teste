<?php

namespace OGestor\Models\Contacts;

use OGestor\Models\Clients\ClientModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CelphoneModel extends Model
{
	use SoftDeletes;
	
	protected $table = 'celphones';
	
	protected $fillable = [
		'number',
		'prefix',
	];
	
	protected $casts = [
		'number' => 'integer',
		'prefix' => 'integer',
	];
	
	/**
	 * @return string
	 * the model base has a number property and override own number property
	 */
	public function getNumberAttribute ()
	{
		return $this->attributes['number'];
	}
	
	public function setNumberAttribute (string $value)
	{
		$this->attributes['number'] = $value;
	}
	
	public function client ()
	{
		return $this->belongsTo(ClientModel::class, 'client_id', 'id');
	}
}
