<?php

namespace OGestor\Models\Reports;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportModel extends Model
{
	use SoftDeletes;
	
	protected $table = 'report_observers';
	
	protected $fillable = [
		'class',
		'event',
		'data',
		'class_id',
	];
	
	protected $casts = [
		'class_id' 	=> 'integer',
		'data'		=> 'array'
	];
	
	public function scopeEvent ($query, string $event)
	{
		return $query->where('event', $event);
	}
}
