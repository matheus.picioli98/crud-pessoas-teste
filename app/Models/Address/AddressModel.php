<?php

namespace OGestor\Models\Address;

use OGestor\Models\Clients\ClientModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddressModel extends Model
{
	use SoftDeletes;
	
	protected $table = 'address';
	
	protected $fillable = [
		'street',
		'number',
		'neighborhood',
		'notes',
	];
	
	protected $casts = [
		'number' => 'integer',
	];
	
	public function client ()
	{
		return $this->belongsTo(ClientModel::class, 'client_id', 'id');
	}
}
