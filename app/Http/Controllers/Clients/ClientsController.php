<?php

namespace OGestor\Http\Controllers\Clients;

use OGestor\Http\Requests\Clients\ClientCreateRequest;
use OGestor\Models\Address\AddressModel;
use OGestor\Models\Clients\ClientModel;
use OGestor\Http\Controllers\Controller;
use OGestor\Models\Contacts\CelphoneModel;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param string $active_slug
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ($active_slug = 'ativos')
	{
		$value = true;
		if ($active_slug === 'inativos') {
			$value = false;
		}
		
		$clients = ClientModel::actives($value)->get();
		return view('clients.list')
			->with('clients', $clients);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		return view('clients.form');
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param ClientCreateRequest $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store (ClientCreateRequest $request)
	{
		/**
		 * @todo add the data on database.
		 * @todo from a eloquent model
		 */
		$client = ClientModel::create($request->except('_token'));
		foreach ($request->get('telephones') as $telephone) {
			$celphone = new CelphoneModel;
			$celphone->fill([
				'number' => $telephone
			]);
			$celphone->client()->associate($client);
			$celphone->save();
		}
		foreach ($request->get('address') as $address) {
			$addressModel = new AddressModel;
			$addressModel->fill([
				'street' 		=> $address['street'],
				'number' 		=> $address['number'],
				'neighborhood' 	=> $address['neighborhood'],
			]);
			$addressModel->client()->associate($client);
			$addressModel->save();
		}
		return redirect()->route('clients.index');
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show ($id)
	{
		$client = ClientModel::findOrFail($id);
		return view('clients.show')
			->with('client', $client);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit ($id)
	{
		$client = ClientModel::findOrFail($id);
		return view('clients.form')
			->with('client', $client);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param  int              $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $id)
	{
		$client = ClientModel::findOrFail($id);
		$client->update($request->except(['_token', '_method']));
		
		if ($request->has('active')) {
			return redirect()->route('clients.index');
		}
		
		foreach ($request->get('telephones') as $telephone) {
			$celphone = CelphoneModel::findOrFail($telephone['id']);
			$celphone->update([
				'number' => $telephone['number']
			]);
		}
		
		foreach ($request->get('address') as $address) {
			$addressModel = AddressModel::findOrFail($address['id']);
			$addressModel->update([
				'street' 		=> $address['street'],
				'number' 		=> $address['number'],
				'neighborhood' 	=> $address['neighborhood'],
			]);
		}
		
		return redirect()->route('clients.index');
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($id)
	{
		/**
		 * @todo find and delete the client model
		 */
		$client = ClientModel::findOrFail($id);
		$client->delete();
		return redirect()->route('clients.index');
	}
}
