<?php

namespace OGestor\Http\Requests\Clients;

use Illuminate\Foundation\Http\FormRequest;

class ClientCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'name'		=> 'required',
			'last_name'	=> 'required',
			'nickname'	=> '',
			'birthday'	=> 'required|date',
			'telephones'=> 'array',
			'address'	=> 'array',
        ];
    }
    
    public function messages ()
	{
		return [
			'name.required' 		=> 'O nome é obrigatório',
			'last_name.required'	=> 'O sobrenome é obrigatório',
			'birthday.required'		=> 'A data de nascimento é obrigatório',
		];
	}
}
