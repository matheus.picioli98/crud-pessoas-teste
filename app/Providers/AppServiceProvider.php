<?php

namespace OGestor\Providers;

use OGestor\Models\Address\AddressModel;
use OGestor\Models\Clients\ClientModel;
use OGestor\Models\Contacts\CelphoneModel;
use OGestor\Observers\AddressObserver;
use OGestor\Observers\CelphoneObserver;
use OGestor\Observers\ClientObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		Schema::defaultStringLength(191);
		ClientModel::observe(ClientObserver::class);
		AddressModel::observe(AddressObserver::class);
		CelphoneModel::observe(CelphoneObserver::class);
    }
}
