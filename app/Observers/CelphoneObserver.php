<?php

namespace OGestor\Observers;

use OGestor\Models\Contacts\CelphoneModel;
use OGestor\Models\Reports\ReportModel;

class CelphoneObserver
{
	/**
	 * Handle the celphone model "created" event.
	 *
	 * @param  \OGestor\Models\Contacts\CelphoneModel $celphoneModel
	 *
	 * @return void
	 */
	public function created (CelphoneModel $celphoneModel)
	{
		ReportModel::create([
			'class' 	=> CelphoneModel::class,
			'class_id' 	=> $celphoneModel->id,
			'event' 	=> 'created',
			'data' 		=> $celphoneModel->toJson(),
		]);
	}
	
	/**
	 * Handle the celphone model "updated" event.
	 *
	 * @param  \OGestor\Models\Contacts\CelphoneModel $celphoneModel
	 *
	 * @return void
	 */
	public function updated (CelphoneModel $celphoneModel)
	{
		ReportModel::create([
			'class' 	=> CelphoneModel::class,
			'class_id' 	=> $celphoneModel->id,
			'event' 	=> 'updated',
			'data' 		=> $celphoneModel->toJson(),
		]);
	}
	
	/**
	 * Handle the celphone model "deleted" event.
	 *
	 * @param  \OGestor\Models\Contacts\CelphoneModel $celphoneModel
	 *
	 * @return void
	 */
	public function deleted (CelphoneModel $celphoneModel)
	{
		ReportModel::create([
			'class' 	=> CelphoneModel::class,
			'class_id' 	=> $celphoneModel->id,
			'event' 	=> 'deleted',
			'data' 		=> $celphoneModel->toJson(),
		]);
	}
}
