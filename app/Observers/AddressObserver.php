<?php

namespace OGestor\Observers;

use OGestor\Models\Address\AddressModel;
use OGestor\Models\Reports\ReportModel;

class AddressObserver
{
	/**
	 * Handle the address model "created" event.
	 *
	 * @param  \OGestor\Models\Address\AddressModel $addressModel
	 *
	 * @return void
	 */
	public function created (AddressModel $addressModel)
	{
		ReportModel::create([
			'class' 	=> AddressModel::class,
			'class_id' 	=> $addressModel->id,
			'event' 	=> 'created',
			'data' 		=> $addressModel->toJson(),
		]);
	}
	
	/**
	 * Handle the address model "updated" event.
	 *
	 * @param  \OGestor\Models\Address\AddressModel $addressModel
	 *
	 * @return void
	 */
	public function updated (AddressModel $addressModel)
	{
		ReportModel::create([
			'class' 	=> AddressModel::class,
			'class_id' 	=> $addressModel->id,
			'event' 	=> 'updated',
			'data' 		=> $addressModel->toJson(),
		]);
	}
	
	/**
	 * Handle the address model "deleted" event.
	 *
	 * @param  \OGestor\Models\Address\AddressModel $addressModel
	 *
	 * @return void
	 */
	public function deleted (AddressModel $addressModel)
	{
		ReportModel::create([
			'class' 	=> AddressModel::class,
			'class_id' 	=> $addressModel->id,
			'event' 	=> 'deleted',
			'data' 		=> $addressModel->toJson(),
		]);
	}
}
