<?php

namespace OGestor\Observers;

use OGestor\Models\Clients\ClientModel;
use OGestor\Models\Reports\ReportModel;

class ClientObserver
{
	/**
	 * Handle the client model "created" event.
	 *
	 * @param  ClientModel $clientModel
	 *
	 * @return void
	 */
	public function created (ClientModel $clientModel)
	{
		ReportModel::create([
			'class' 	=> ClientModel::class,
			'class_id'	=> $clientModel->id,
			'event' 	=> 'created',
			'data'		=> $clientModel->toJson()
		]);
	}
	
	/**
	 * Handle the client model "updated" event.
	 *
	 * @param  ClientModel $clientModel
	 *
	 * @return void
	 */
	public function updated (ClientModel $clientModel)
	{
		ReportModel::create([
			'class' 	=> ClientModel::class,
			'class_id'	=> $clientModel->id,
			'event' 	=> 'updated',
			'data'		=> $clientModel->toJson()
		]);
	}
	
	/**
	 * Handle the client model "deleted" event.
	 *
	 * @param  ClientModel $clientModel
	 *
	 * @return void
	 */
	public function deleted (ClientModel $clientModel)
	{
		ReportModel::create([
			'class' 	=> ClientModel::class,
			'class_id'	=> $clientModel->id,
			'event' 	=> 'deleted',
			'data'		=> $clientModel->toJson()
		]);
	}
}
