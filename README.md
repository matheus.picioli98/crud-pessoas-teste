## CRUD PESSOAS - Passo a passo

1. Clonar o projeto, para isso podemos executar o seguinte comando:
    - ``git clone https://gitlab.com/matheus.picioli98/crud-pessoas-teste ogestor-teste``
1. Entrar na pasta ``ogestor-teste``
1. Rodar o comando: ``composer install``
1. Configurar arquivo ``.env``, para isso vamos fazer:
    - Copie o arquivo ``.env.example`` e renomeie para ``.env``, ou ``cp .env.example .env``
    - Configure as variaveis de banco de dados, que tem o prefixo ``DB_``, para as suas configurações locais
    - Rode o comando ``php artisan key:generate`` para gerar uma chave de segurança na sua aplicação.
    - Rode o comando ``php artisan config:cache`` para aplicar as alterações realizadas no ``.env``
1. Crie o banco de dados informado no ``.env``, no meu caso eu chamei de ``ogestor_crud``
    - ``CREATE DATABASE ogestor_crud``
1. Criar as tabelas e registros iniciais, para isso rode os comandos:
    - ``php artisan migrate``
    - ``php artisan db:seed``
1. Agora basta startar o servidor de desenvolvimento
    - ``php artisan serve``
    - Acessar o endereço ``localhost:8000`` no seu navegador para ver a aplicação rodando
    
    
### Bônus

> Cada evento dos observers cria um registro na tabela ``report_observers``.

Podemos ver esses registros em forma de um log simples acessando a rota ``log/listar``, aonde poderá ver todos os dados inseridos, alterados ou excluídos no banco de dados da aplicação.

PS: esta rota não está no menu lateral esquerdo. 