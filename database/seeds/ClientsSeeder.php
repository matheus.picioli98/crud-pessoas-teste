<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use OGestor\Models\Address\AddressModel;
use OGestor\Models\Clients\ClientModel;
use OGestor\Models\Contacts\CelphoneModel;

class ClientsSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run ()
	{
		$matheus = ClientModel::create([
			'name'		=> 'Matheus',
			'last_name'	=> 'Picioli',
			'nickname'	=> 'Rogerin',
			'birthday'	=> Carbon::create(1998, 10, 8),
		]);
		
		$felipe = ClientModel::create([
			'name'		=> 'Felipe',
			'last_name'	=> 'Trindade',
			'nickname'	=> 'Cuevinha',
			'birthday'	=> Carbon::create(1950, 10, 8),
		]);
		
		$matheusAddress = new AddressModel;
		$matheusAddress->fill([
			'street'		=> 'Anis Trabulsi',
			'number'		=> 325,
			'neighborhood'	=> 'Solo Sagrado',
		]);
		$matheusAddress->client()->associate($matheus);
		$matheusAddress->save();
		
		$felipeAddress = new AddressModel;
		$felipeAddress->fill([
			'street'		=> 'Dos bobos',
			'number'		=> 0,
			'neighborhood'	=> 'S/B',
		]);
		$felipeAddress->client()->associate($felipe);
		$felipeAddress->save();
		
		$matheusPhone = new CelphoneModel;
		$matheusPhone->fill([
			'number' => '17 982064024'
		]);
		$matheusPhone->client()->associate($matheus);
		$matheusPhone->save();
		
		$felipePhone = new CelphoneModel;
		$felipePhone->fill([
			'number' => '17 981745101'
		]);
		$felipePhone->client()->associate($felipe);
		$felipePhone->save();
	}
}
