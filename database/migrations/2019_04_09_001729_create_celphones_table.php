<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCelphonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('celphones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number', 20);
            $table->smallInteger('prefix')->nullable();
            
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')
				->references('id')->on('clients')
				->onDelete('cascade');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('celphones');
    }
}
